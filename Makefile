JAVA_HOME=$(shell dirname $$(dirname $$(readlink -f $$(which javac))))
GRAAL_HOME=/usr/lib/jvm/java-11-graalvm

BIN_DIR=bin
BUILD_DIR=build
BIN_NAME=strix
OWL_LIB_DIR=native-library
OWL_LIB_NAME=libowl.so

all: $(BIN_DIR)/$(BIN_NAME)

clean:
	rm -rf $(BUILD_DIR)

clean-owl:
	rm -rf $(BIN_DIR)/$(OWL_LIB_NAME) $(BUILD_DIR)/$(OWL_LIB_DIR)

distclean: clean
	rm -rf $(BIN_DIR)

$(BIN_DIR):
	mkdir -p $(BIN_DIR)

$(BUILD_DIR):
	mkdir -p $(BUILD_DIR)

$(BUILD_DIR)/$(OWL_LIB_DIR)/$(OWL_LIB_NAME): lib/owl | $(BUILD_DIR)
	JAVA_HOME=$(JAVA_HOME) GRAAL_HOME=$(GRAAL_HOME) GRADLE_OPTS=-Dorg.gradle.project.buildDir=../../$(BUILD_DIR) lib/owl/gradlew distZip -Pdisable-pandoc -plib/owl --project-cache-dir=../../$(BUILD_DIR)/.gradle

$(BIN_DIR)/$(OWL_LIB_NAME): $(BUILD_DIR)/$(OWL_LIB_DIR)/$(OWL_LIB_NAME) | $(BIN_DIR)
	cp $< $@

$(BUILD_DIR)/Makefile: $(BIN_DIR)/$(OWL_LIB_NAME) | $(BUILD_DIR)
	(cd $(BUILD_DIR) && cmake -G"Unix Makefiles" -DCMAKE_BUILD_TYPE=Release ..)

$(BIN_DIR)/$(BIN_NAME): $(BUILD_DIR)/Makefile
	@ $(MAKE) -C $(BUILD_DIR) --no-print-directory

test: $(BIN_DIR)/$(BIN_NAME) $(BUILD_DIR)/Makefile
	(cd $(BUILD_DIR) && ctest)

.PHONY: all clean clean-owl distclean test $(BIN_DIR)/$(BIN_NAME)
