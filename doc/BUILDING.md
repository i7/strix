# Building

## Dependencies

 - C++ compiler supporting C++17 and OpenMP, e.g. [GCC](https://gcc.gnu.org/) 8 and later.
 - [GNU Make](https://www.gnu.org/software/make/).
 - [Boost](http://www.boost.org/) 1.53 or higher with the following libraries: `program options`, `filesystem`, `iostreams` and `system`.
 - [zlib](http://www.zlib.net/).
 - [CMake](https://cmake.org/) 3.11 or higher.
 - [GraalVM](https://www.graalvm.org/) for JDK 11 or higher, with `native-image` binary

**Ubuntu 19.10**

On Ubuntu 19.10, all dependencies can be installed as follows:
```
sudo apt install g++ cmake make libboost-dev libboost-program-options-dev libboost-filesystem-dev libboost-iostreams-dev zlib1g-dev openjdk-13-jdk
```
For the GraalVM, download the [latest release](https://github.com/oracle/graal/releases)
and unpack it to `/usr/lib/jvm`.
Then install the `native-image` binary as follows:
```
sudo /usr/lib/jvm/java-11-graalvm/bin/gu install native-image
```

**Arch Linux**

On Arch Linux, most dependencies can be installed as follows:
```
sudo pacman -S gcc cmake make boost boost-libs zlib jdk-openjdk
sudo archlinux-java set java-13-openjdk
```
For the GraalVM, the package `jdk11-graalvm-bin` from the [Arch User Repository](https://aur.archlinux.org/packages/jdk11-graalvm-bin) needs to be installed.
Then install the `native-image` binary as follows:
```
sudo /usr/lib/jvm/java-11-graalvm/bin/gu install native-image
```

**CentOS 7**

On CentOS 7, first a newer version of the build tools need to be installed:
```
sudo yum install cmake3 devtoolset-8
```
For the GraalVM, download the [latest release](https://github.com/oracle/graal/releases)
and unpack it to `/usr/lib/jvm`.
Then install the `native-image` binary as follows:
```
sudo /usr/lib/jvm/java-11-graalvm/bin/gu install native-image
```

As no JDK 11 is available by default, change the following two lines
at the top of the `Makefile` file:
```
JAVA_HOME=/usr/lib/jvm/java-11-graalvm
GRAAL_HOME=/usr/lib/jvm/java-11-graalvm
```
Further, since the default version of CMake is too old, change
the single occurrence of `cmake` to `cmake3` in `Makefile`.

Finally, before invoking `make` in the build process, execute the
following command to change to a shell where GCC 8 is available:
```
scl enable devtoolset-8 -- bash
```

## Compilation

If the repository has been cloned with git, first the submodules have to be initialized.
If Strix has been obtained as a release zip file, this step can be skipped.
```
git submodule init
git submodule update
```

The compilation process can be started by:
```
make
```

After compilation, the folder `bin` will be created and should contain the executable `strix` and the library `libowl.so`.
To use Strix, the executable `strix` has to be invoked, but the `libowl.so` library has to remain in the same directory or in the library path.

## Optional dependencies

To run the test suite for Strix (`make test`) or the benchmarks (`benchmarks/run_benchmarks.sh`),
additional dependencies are needed to convert input files and verify the output:

- [SyfCo](https://github.com/meyerphi/syfco) for TLSF conversion.
- [combine-aiger](https://github.com/meyerphi/combine-aiger) for combining specification and implementation.
- [NuSMV](http://nusmv.fbk.eu/index.html) with `ltl2smv` binary in version 2.6.0.
- [nuXmv](https://es-static.fbk.eu/tools/nuxmv/index.php) model checker in version 2.0.0.
- [AIGER tools](http://fmv.jku.at/aiger/) with `smvtoaig` binary in version 1.9.4 or higher.

The [install dependencies script](scripts/install_dependencies.sh) can be
used to install all these dependencies, but may need to be adapted for other systems.
