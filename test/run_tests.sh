#!/bin/bash

# exit on error
set -e
# echo commands
set -x
# break when pipe fails
set -o pipefail

# tool paths
STRIX=$1
ROOT_DIR=$2

# input file
SPECIFICATION=$3

if [ ${SPECIFICATION: -5} == ".tlsf" ]; then
  BASE=$(basename ${SPECIFICATION%.tlsf})
  TLSF=true
elif [ ${SPECIFICATION: -5} == ".ehoa" ]; then
  BASE=$(basename ${SPECIFICATION%.ehoa})
  EHOA=true
else
    echo "Unknown specification format: $SPECIFICATION"
    exit 1
fi

# type of test
TEST=$4

# temporary files
BASE=$(basename ${2%.tlsf})
IMPLEMENTATION=/tmp/$BASE.aag

STRIX_OPTIONS='--auto'
STRIX_TLSF_OPTIONS='-e pq -c'
STRIX_EHOA_OPTIONS='--hoa'

# run the tool
if [ "$TLSF" = true ]; then
    # get formula, inputs and outputs from specification using syfco
    LTL=$(syfco -f ltl -q double -m fully $SPECIFICATION)
    INS=$(syfco --print-input-signals $SPECIFICATION)
    OUTS=$(syfco --print-output-signals $SPECIFICATION)

    RESULT=$($STRIX $STRIX_TLSF_OPTIONS $STRIX_OPTIONS -f "$LTL" --ins "$INS" --outs "$OUTS" -o $IMPLEMENTATION)
elif [ "$EHOA" = true ]; then
    RESULT=$($STRIX $STRIX_EHOA_OPTIONS $STRIX_OPTIONS $SPECIFICATION -o $IMPLEMENTATION)
fi

if [ "$TEST" == "REALIZABLE" ] || [ "$TEST" == "UNREALIZABLE" ]; then
    # check if tool answers correctly (and nothing else)
    if [ "$TEST" != "$RESULT" ]; then
        echo "Incorrect result for $TEST: $RESULT"
        exit 1
    fi

    # verify solution (with time limit of 10 seconds)
    $ROOT_DIR/scripts/verify.sh $IMPLEMENTATION $SPECIFICATION $TEST 10

    # remove solution
    rm -f $IMPLEMENTATION
else
    # unknown test
    echo "Unknown test: $TEST"
    exit 2
fi
