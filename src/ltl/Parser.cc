#include "Parser.h"
#include "util/String.h"

#include <iostream>
#include <fstream>
#include <sstream>
#include <algorithm>

namespace ltl {

Parser::Parser(JVM owl) : owl(owl) { }

Parser::~Parser() { }

std::string Parser::parse_file(const std::string& input_file) {
    // read input file
    std::string input_string;

    std::ifstream infile(input_file);
    if (infile.is_open()) {
        input_string = std::string((std::istreambuf_iterator<char>(infile)), std::istreambuf_iterator<char>());
    }
    else {
        throw std::runtime_error("could not open input file");
    }
    if (infile.bad()) {
        throw std::runtime_error("could not read input file");
    }
    infile.close();

    return input_string;
}

Specification Parser::parse_string(const std::string& input_string, const int verbosity) const {
    if (verbosity >= 4) {
        std::cout << "Input: " << std::endl << input_string << std::endl;
    }
    Specification spec = parse_string_with_owl(owl, input_string);
    if (verbosity >= 2) {
        if (spec.owl_object != nullptr) {
            std::cout << "Object: " << owl_object_to_string(owl, spec.owl_object) << std::endl;
        }
        std::cout << "Inputs:";
        for (const auto& i : spec.inputs) {
            std::cout << " " << i;
        }
        std::cout << std::endl;
        std::cout << "Outputs:";
        for (const auto& o : spec.outputs) {
            std::cout << " " << o;
        }
        std::cout << std::endl;
    }

    return spec;
}

}
