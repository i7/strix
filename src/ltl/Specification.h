#pragma once

#include <string>

#include "owltypes.h"
#include "libowl.h"

#include "Definitions.h"

namespace ltl {

enum class SpecificationType : uint8_t {
    FORMULA,
    AUTOMATON,
};

class Specification {
private:
    Specification(const Specification&) = delete;
    Specification& operator=(const Specification&) = delete;
public:
    JVM owl;
    void* owl_object;
    SpecificationType type;
    std::vector<std::string> inputs;
    std::vector<std::string> outputs;

    Specification(
        JVM owl,
        void* owl_object,
        SpecificationType type,
        std::vector<std::string> inputs,
        std::vector<std::string> outputs
    ) :
        owl(owl),
        owl_object(owl_object),
        type(type),
        inputs(inputs),
        outputs(outputs)
    {}
    Specification(Specification&& other) :
        owl(other.owl),
        owl_object(other.owl_object),
        type(std::move(other.type)),
        inputs(std::move(other.inputs)),
        outputs(std::move(other.outputs))
    {
        other.owl_object = nullptr;
    }

    ~Specification() {
        if (owl_object != nullptr) {
            destroy_object_handle(owl, owl_object);
        }
    }
};

}
