#pragma once

#include "Parser.h"

namespace ltl {

class HOAParser : public Parser {
protected:
    Specification parse_string_with_owl(
            JVM owl,
            const std::string& text
    ) const;
public:
    HOAParser(JVM owl);
    ~HOAParser();
};

}
