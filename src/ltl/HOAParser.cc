#include "HOAParser.h"

#include <iostream>
#include <sstream>
#include <algorithm>
#include <boost/algorithm/string.hpp>
#include <boost/algorithm/string/predicate.hpp>

namespace ltl {

HOAParser::HOAParser(JVM owl) :
    Parser(owl)
{ }

HOAParser::~HOAParser() { }

Specification HOAParser::parse_string_with_owl(
        JVM owl,
        const std::string& text
) const {
    OwlAutomaton automaton = automaton_parse(owl, const_cast<char*>(text.c_str()));
    std::vector<std::string> inputs;
    std::vector<std::string> outputs;

    int num_inputs = automaton_atomic_propositions_uncontrollable_size(owl, automaton);
    int alphabet_size = automaton_atomic_propositions(owl, automaton);
    size_t buffer_size = text.size();
    char* buffer = new char[buffer_size];
    for (int i = 0; i < alphabet_size; i++) {
        size_t len = automaton_atomic_propositions_label(owl, automaton, i, buffer, buffer_size);
        assert(len < buffer_size);
        std::string ap(buffer, len);
        if (i < num_inputs) {
            inputs.push_back(ap);
        }
        else {
            outputs.push_back(ap);
        }
    }
    delete[] buffer;

    return Specification(owl, automaton, SpecificationType::AUTOMATON, std::move(inputs), std::move(outputs));
}

}

