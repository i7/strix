#pragma once

#include <string>

#include "Definitions.h"
#include "ltl/Specification.h"

#include "owltypes.h"

namespace ltl {

class Parser {
private:
    JVM owl;

protected:
    Parser(JVM owl);

    virtual Specification parse_string_with_owl(
            JVM owl,
            const std::string& text
    ) const = 0;

public:
    virtual ~Parser();

    std::string parse_file(const std::string& input_file);
    Specification parse_string(const std::string& input_string, const int verbosity = 0) const;
};

}
