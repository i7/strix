#include "LTLParser.h"

#include <iostream>
#include <sstream>
#include <algorithm>

namespace ltl {

LTLParser::LTLParser(JVM owl, const std::vector<std::string>& inputs, const std::vector<std::string>& outputs, const std::string finite) :
    Parser(owl),
    inputs(inputs),
    outputs(outputs),
    finite(finite)
{ }

LTLParser::~LTLParser() { }

Specification LTLParser::parse_string_with_owl(
        JVM owl,
        const std::string& text
) const {
    // Create list of variables for parser
    int num_vars = inputs.size() + outputs.size();
    char** variables = new char*[num_vars];
    for (size_t i = 0; i < inputs.size(); i++) {
        variables[i] = const_cast<char*>(inputs[i].c_str());
    }
    for (size_t i = 0; i < outputs.size(); i++) {
        variables[i + inputs.size()] = const_cast<char*>(outputs[i].c_str());
    }

    // Parse with provided variables
    const bool finite_semantics = !finite.empty();
    OwlFormula formula;
    if (finite_semantics) {
        formula = ltl_formula_parse_with_finite_semantics(owl, const_cast<char*>(text.c_str()), variables, num_vars);
    }
    else {
        formula = ltl_formula_parse(owl, const_cast<char*>(text.c_str()), variables, num_vars);
    }
    delete[] variables;

    // Create list of variables for specification
    std::vector<std::string> spec_inputs = inputs;
    std::vector<std::string> spec_outputs = outputs;
    if (finite_semantics) {
        if (std::find(inputs.begin(), inputs.end(), finite) != inputs.end() || std::find(outputs.begin(), outputs.end(), finite) != outputs.end()) {
            std::stringstream message;
            message << "Proposition '" << finite << "' for LTLf transformation already appears in list of propositions. ";
            message << "Please rename it manually by giving a different argument to the '--from-ltlf' option.";
            throw std::invalid_argument(message.str());
        }
        spec_outputs.emplace_back(finite);
    }

    // Return specification with input and output variables
    return Specification(owl, formula, SpecificationType::FORMULA,  std::move(spec_inputs), std::move(spec_outputs));
}

}
