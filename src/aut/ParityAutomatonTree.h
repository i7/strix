#pragma once

#include <vector>
#include <deque>
#include <map>
#include <set>
#include <memory>
#include <limits>
#include <iostream>

#include "cuddObj.hh"
#include "owltypes.h"

#include "Definitions.h"
#include "Automaton.h"

namespace aut {

typedef product_state_t::const_iterator state_iter;

class ParityAutomatonTree {
    protected:
        size_t state_index;
        size_t state_width;

        ParityAutomatonTree(const NodeType node_type, const Parity parity, const color_t max_color) :
            state_index(0), node_type(node_type), parity(parity), max_color(max_color)
        {}

    public:
        virtual ~ParityAutomatonTree() {}

        const NodeType node_type;
        const Parity parity;
        const color_t max_color;

        virtual void getInitialState(product_state_t& state) = 0;
        virtual ColorScore getSuccessor(const product_state_t& state, product_state_t& new_state, letter_t letter) = 0;

        virtual BDD computeSafetyFilter(Cudd manager, size_t var_begin, size_t var_end, int depth) = 0;

        virtual void setState(product_state_t& new_state, node_id_t state) = 0;
        virtual void setTopState(product_state_t& new_state) = 0;
        virtual void setBottomState(product_state_t& new_state) = 0;

        virtual bool isTopState(const product_state_t& state) const = 0;
        virtual bool isBottomState(const product_state_t& state) const = 0;

        inline size_t getStateIndex() const { return state_index; }

        virtual int getMinIndex() const = 0;
        virtual letter_t getMaximumAlphabetSize() const = 0;
        virtual std::set<letter_t> getAlphabet() const = 0;

        inline void print_type() const {
            switch(node_type) {
                case NodeType::PARITY:
                    std::cout << "Parity " << ((parity == Parity::EVEN) ? "even" : "odd");
                    break;
                case NodeType::BUCHI:    std::cout << "Büchi";    break;
                case NodeType::CO_BUCHI: std::cout << "co-Büchi"; break;
                case NodeType::WEAK:     std::cout << "Weak";     break;
            }
        }
        virtual void print(const int verbosity = 0, const int indent = 0) const = 0;
};

class ParityAutomatonTreeLeaf : public ParityAutomatonTree {
    friend class AutomatonTreeStructure;

    private:
        ParityAutomatonTreeLeaf(const ParityAutomatonTreeLeaf&) = delete;

        Automaton& automaton;
        const std::string name;
        const int reference;
        std::vector<std::pair<letter_t,letter_t>> mapping;

    protected:
        ParityAutomatonTreeLeaf(
            Automaton& automaton,
            const std::string name,
            const int reference,
            std::vector<std::pair<letter_t,letter_t>> mapping
        );

    public:
        virtual ~ParityAutomatonTreeLeaf();
        ParityAutomatonTreeLeaf(ParityAutomatonTreeLeaf&&) = default;

        void getInitialState(product_state_t& state) override;
        ColorScore getSuccessor(const product_state_t& state, product_state_t& new_state, letter_t letter) override;

        BDD computeSafetyFilter(Cudd manager, size_t var_begin, size_t var_end, int depth);

        void setState(product_state_t& new_state, node_id_t state) override;
        void setTopState(product_state_t& new_state) override;
        void setBottomState(product_state_t& new_state) override;

        bool isTopState(const product_state_t& state) const override;
        bool isBottomState(const product_state_t& state) const override;

        virtual int getMinIndex() const override;
        virtual letter_t getMaximumAlphabetSize() const override;
        virtual std::set<letter_t> getAlphabet() const override;

        void print(const int verbosity = 0, const int indent = 0) const override;
};

class ParityAutomatonTreeNode : public ParityAutomatonTree {
    friend class AutomatonTreeStructure;

    protected:
        const node_type_t tag;
        const std::vector<std::unique_ptr<ParityAutomatonTree>> children;
        const bool parity_child;
        const color_t dp;
        const node_id_t round_robin_size;

        ParityAutomatonTreeNode(
            const node_type_t tag, const NodeType node_type, const Parity parity, const color_t max_color,
            const node_id_t round_robin_size, const bool parity_child,
            const color_t dp, std::vector<std::unique_ptr<ParityAutomatonTree>> children
        );

    public:
        virtual ~ParityAutomatonTreeNode();

        virtual void getInitialState(product_state_t& state) override;
        virtual ColorScore getSuccessor(const product_state_t& state, product_state_t& new_state, letter_t letter) override;

        virtual BDD computeSafetyFilter(Cudd manager, size_t var_begin, size_t var_end, int depth);

        virtual void setState(product_state_t& new_state, node_id_t state) override;
        virtual void setTopState(product_state_t& new_state) override;
        virtual void setBottomState(product_state_t& new_state) override;

        virtual bool isTopState(const product_state_t& state) const override;
        virtual bool isBottomState(const product_state_t& state) const override;

        virtual int getMinIndex() const override;
        virtual letter_t getMaximumAlphabetSize() const override;
        virtual std::set<letter_t> getAlphabet() const override;

        void print(const int verbosity = 0, const int indent = 0) const override;
};

class ParityAutomatonTreeBiconditionalNode : public ParityAutomatonTreeNode {
    friend class AutomatonTreeStructure;

    private:
        const int parity_child_index;
        const color_t d1;
        const color_t d2;

    protected:
        ParityAutomatonTreeBiconditionalNode(
            const NodeType node_type, const Parity parity, const color_t max_color,
            const node_id_t round_robin_size, const bool parity_child, const color_t dp,
            const int parity_child_index,
            std::vector<std::unique_ptr<ParityAutomatonTree>> children
        );

    public:
        virtual ~ParityAutomatonTreeBiconditionalNode();

        void getInitialState(product_state_t& state) override;
        ColorScore getSuccessor(const product_state_t& state, product_state_t& new_state, letter_t letter) override;

        BDD computeSafetyFilter(Cudd manager, size_t var_begin, size_t var_end, int depth);

        void setState(product_state_t& new_state, node_id_t state) override;
        void setTopState(product_state_t& new_state) override;
        void setBottomState(product_state_t& new_state) override;

        bool isTopState(const product_state_t& state) const override;
        bool isBottomState(const product_state_t& state) const override;
};

class AutomatonTreeStructure {
    private:
        AutomatonTreeStructure(const AutomatonTreeStructure&) = delete;
        AutomatonTreeStructure(JVM owl, size_t alphabet_size);

        JVM owl;
        OwlDecomposedDPA owl_automaton;
        size_t alphabet_size;
        std::deque<Automaton> automata;
        std::unique_ptr<ParityAutomatonTree> tree;
        std::vector<atomic_proposition_status_t> statuses;

        std::unique_ptr<ParityAutomatonTree> constructTree(const OwlStructure tree, std::vector<ParityAutomatonTreeLeaf*>& leaves);
        std::vector<ParityAutomatonTreeLeaf*> leaves;

        std::vector<size_t> leaf_state_indices;
        product_state_t initial_state;

    public:
        static AutomatonTreeStructure fromFormula(Isolate isolate, JVM owl, OwlFormula formula, size_t num_inputs, size_t num_outputs, bool simplify);
        static AutomatonTreeStructure fromAutomaton(Isolate isolate, JVM owl, OwlDecomposedDPA automaton, size_t num_inputs, size_t num_outputs);

        AutomatonTreeStructure(AutomatonTreeStructure&&);
        ~AutomatonTreeStructure();

        Parity getParity() const;
        color_t getMaxColor() const;

        product_state_t getInitialState() const;
        ColorScore getSuccessor(const product_state_t& state, product_state_t& new_state, letter_t letter);

        BDD computeSafetyFilter(Cudd manager, size_t var_begin, size_t var_end);

        std::vector<int> getAutomatonStates(const product_state_t& state) const;
        bool declareWinning(const product_state_t& state, const Player winner);
        Player queryWinner(const product_state_t& state);

        bool isTopState(const product_state_t& state) const;
        bool isBottomState(const product_state_t& state) const;

        std::set<letter_t> getAlphabet() const;
        atomic_proposition_status_t getVariableStatus(int variable) const;

        void print(const int verbosity = 0) const;
        void print_memory_usage() const;
};

}
