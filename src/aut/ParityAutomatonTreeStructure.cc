#include "ParityAutomatonTree.h"

#include <iostream>
#include <iomanip>
#include <algorithm>
#include <cmath>

#include "util/String.h"

namespace aut {

bool cmp_trees(const std::unique_ptr<ParityAutomatonTree>& t1, const std::unique_ptr<ParityAutomatonTree>& t2) {
    if (t1->node_type < t2->node_type) {
        return true;
    }
    else if (t1->node_type > t2->node_type) {
        return false;
    }
    else {
        const letter_t size1 = t1->getMaximumAlphabetSize();
        const letter_t size2 = t2->getMaximumAlphabetSize();
        if (size1 < size2) {
            return true;
        }
        else if (size1 > size2) {
            return false;
        }
        else {
            const auto alphabet1 = t1->getAlphabet();
            const auto alphabet2 = t2->getAlphabet();
            if (alphabet1 < alphabet2) {
                return true;
            }
            else if (alphabet1 > alphabet2) {
                return false;
            }
            else {
                return t1->getMinIndex() < t2->getMinIndex();
            }
        }
    }
}

AutomatonTreeStructure::AutomatonTreeStructure(JVM owl, size_t alphabet_size) :
    owl(owl),
    alphabet_size(alphabet_size)
{
    statuses.reserve(alphabet_size);
    for (size_t i = 0; i < alphabet_size; i++) {
        statuses.push_back(USED);
    }
}

AutomatonTreeStructure::AutomatonTreeStructure(AutomatonTreeStructure&& other) :
    owl(other.owl),
    owl_automaton(other.owl_automaton),
    alphabet_size(std::move(other.alphabet_size)),
    automata(std::move(other.automata)),
    tree(std::move(other.tree)),
    statuses(std::move(other.statuses)),
    leaves(std::move(other.leaves)),
    leaf_state_indices(std::move(other.leaf_state_indices)),
    initial_state(std::move(other.initial_state))
{
    other.owl_automaton = nullptr;
}

AutomatonTreeStructure AutomatonTreeStructure::fromFormula(Isolate isolate, JVM owl, OwlFormula formula, size_t num_inputs, size_t num_outputs, bool simplify) {
    size_t alphabet_size = num_inputs + num_outputs;
    AutomatonTreeStructure structure(owl, alphabet_size);

    if (simplify) {
        int* statuses = new int[alphabet_size];
        formula = ltl_formula_simplify(owl, formula, num_inputs, statuses, alphabet_size);
        for (size_t i = 0; i < alphabet_size; i++) {
            structure.statuses[i] = (atomic_proposition_status_t)statuses[i];
        }
        delete[] statuses;
    }

    OwlDecomposedDPA decomposed_automaton = decomposed_dpa_of(owl, formula);
    structure.owl_automaton = decomposed_automaton;

    int num_automata = decomposed_dpa_automata_size(owl, decomposed_automaton);

    for (int i = 0; i < num_automata; i++) {
        OwlAutomaton automaton = decomposed_dpa_automata_get(owl, decomposed_automaton, i);
        structure.automata.emplace_back(isolate, owl, automaton);
    }

    OwlStructure owl_structure = decomposed_dpa_structure_get(owl, decomposed_automaton);

    std::vector<ParityAutomatonTreeLeaf*> leaves;
    structure.tree = structure.constructTree(owl_structure, leaves);
    structure.tree->getInitialState(structure.initial_state);
    for (const ParityAutomatonTreeLeaf* leaf : leaves) {
        structure.leaf_state_indices.push_back(leaf->getStateIndex());
    }

    return structure;
}

AutomatonTreeStructure AutomatonTreeStructure::fromAutomaton(Isolate isolate, JVM owl, OwlAutomaton automaton, size_t num_inputs, size_t num_outputs) {
    size_t alphabet_size = num_inputs + num_outputs;
    AutomatonTreeStructure structure(owl, alphabet_size);
    structure.owl_automaton = nullptr;

    structure.automata.emplace_back(isolate, owl, automaton);
    std::string name = "HOA";
    const int reference = 0;
    // create mapping
    std::vector<std::pair<letter_t, letter_t>> mapping;
    mapping.reserve(alphabet_size);
    for (letter_t i = 0; i < alphabet_size; i++) {
        mapping.push_back({i, i});
    }
    // make automaton into tree
    ParityAutomatonTreeLeaf* leaf = new ParityAutomatonTreeLeaf(
            structure.automata[reference],
            name,
            reference,
            mapping
    );
    structure.leaves.push_back(leaf);
    structure.tree = std::unique_ptr<ParityAutomatonTree>(leaf);

    structure.tree->getInitialState(structure.initial_state);
    structure.leaf_state_indices.push_back(structure.tree->getStateIndex());

    return structure;
}

AutomatonTreeStructure::~AutomatonTreeStructure() {
    if (owl_automaton != nullptr) {
        destroy_object_handle(owl, owl_automaton);
    }
}

std::unique_ptr<ParityAutomatonTree> AutomatonTreeStructure::constructTree(OwlStructure tree, std::vector<ParityAutomatonTreeLeaf*>& leaves) {
    const node_type_t tag = decomposed_dpa_structure_node_type(owl, tree);
    if (tag == AUTOMATON) {
        const int reference = decomposed_dpa_structure_referenced_automaton(owl, tree);
        Automaton& automaton = automata[reference];
        // create mapping
        std::vector<std::pair<letter_t, letter_t>> mapping;
        mapping.reserve(alphabet_size);
        for (letter_t i = 0; i < alphabet_size; i++) {
            int j;
            j = decomposed_dpa_structure_referenced_alphabet_mapping(owl, tree, i);
            if (j >= 0) {
                mapping.push_back({i, (letter_t)j});
            }
        }
        // create name
        OwlFormula formula = decomposed_dpa_structure_referenced_formula(owl, tree);
        std::string name = owl_object_to_string(owl, formula);
        destroy_object_handle(owl, formula);
        // move tree into leaf
        ParityAutomatonTreeLeaf* leaf = new ParityAutomatonTreeLeaf(automaton, name, reference, mapping);
        leaves.push_back(leaf);
        return std::unique_ptr<ParityAutomatonTree>(leaf);
    }
    else {
        // defaults for weak nodes
        NodeType node_type = NodeType::WEAK;
        Parity parity = Parity::EVEN;
        color_t max_color = 1;
        node_id_t round_robin_size = 0;

        int parity_child = false;
        color_t parity_child_max_color = 0;
        Parity parity_child_parity = Parity::EVEN;
        int parity_child_index = 0;

        std::vector<std::unique_ptr<ParityAutomatonTree>> children;
        for (int i = 0; i < decomposed_dpa_structure_children(owl, tree); i++) {
            OwlStructure child = decomposed_dpa_structure_get_child(owl, tree, i);

            std::unique_ptr<ParityAutomatonTree> pchild = constructTree(child, leaves);
            if (pchild->node_type == NodeType::PARITY) {
                if (tag == BICONDITIONAL) {
                    parity_child = true;
                }
                else if (parity_child) {
                    throw std::invalid_argument("unsupported automaton tree");
                }
                else {
                    parity_child = true;
                    parity_child_parity = pchild->parity;
                    parity_child_max_color = pchild->max_color;
                }
            }
            else if (pchild->node_type == NodeType::BUCHI) {
                if (tag == CONJUNCTION) {
                    round_robin_size++;
                }
                else if (tag == BICONDITIONAL) {
                    parity_child = true;
                }
            }
            else if (pchild->node_type == NodeType::CO_BUCHI) {
                if (tag == DISJUNCTION) {
                    round_robin_size++;
                }
                else if (tag == BICONDITIONAL) {
                    parity_child = true;
                }
            }

            node_type = join_node_type(node_type, pchild->node_type);

            children.push_back(std::move(pchild));
        }

        // sort children from "small,simple" to "large,complex" as a heuristic for more efficient product construction
        std::sort(children.begin(), children.end(), cmp_trees);

        if (tag == BICONDITIONAL) {
            const NodeType t1 = children[0]->node_type;
            const NodeType t2 = children[1]->node_type;
            node_type = join_node_type_biconditional(t1, t2);

            if (parity_child) {
                // need to treat one child as a parity child and other as child updating the lar

                if (t1 == NodeType::WEAK) {
                    parity_child_index = 1;
                }
                else if (t2 == NodeType::WEAK) {
                    parity_child_index = 0;
                }
                else if (children[0]->max_color < children[1]->max_color) {
                    parity_child_index = 1;
                }
                else {
                    parity_child_index = 0;
                }
                parity_child_parity = children[parity_child]->parity;
                parity_child_max_color = children[parity_child]->max_color;
            }
        }

        if (node_type == NodeType::PARITY) {
            if (tag == CONJUNCTION || tag == DISJUNCTION) {
                if (tag == CONJUNCTION) {
                    parity = Parity::ODD;
                }
                else {
                    parity = Parity::EVEN;
                }
                if (parity_child) {
                    if (parity != parity_child_parity) {
                        parity_child_max_color++;
                    }
                    max_color = parity_child_max_color;
                    if (round_robin_size > 0 && max_color % 2 != 0) {
                        max_color++;
                    }
                }
                else {
                    // needs to have co-buchi and buchi children
                    max_color = 2;
                }
            }
            else {
                if (parity_child) {
                    color_t d1 = children[1 - parity_child_index]->max_color;
                    color_t d2 = children[parity_child_index]->max_color;
                    Parity p1 = children[1 - parity_child_index]->parity;
                    Parity p2 = children[parity_child_index]->parity;

                    if (children[1 - parity_child_index]->node_type == NodeType::WEAK) {
                        // one weak child
                        max_color = d2 + 1;
                        parity = p2;
                        round_robin_size = 0;
                    }
                    else {
                        max_color = d1 + d2;
                        parity = (Parity)(((int)p1 + (int)p2) % 2);
                        round_robin_size = d1;
                    }
                    parity_child_max_color = d2;
                }
                // only weak children otherwise, leave defaults
            }
        }
        else if (node_type == NodeType::BUCHI) {
            parity = Parity::EVEN;
        }
        else if (node_type == NodeType::CO_BUCHI) {
            parity = Parity::ODD;
        }

        // intermediate pointer not needed any more
        destroy_object_handle(owl, tree);

        if (tag == CONJUNCTION || tag == DISJUNCTION) {
            return std::unique_ptr<ParityAutomatonTree>(
                    new ParityAutomatonTreeNode(
                        tag, node_type, parity, max_color, round_robin_size, parity_child, parity_child_max_color, std::move(children)));
        }
        else {
            return std::unique_ptr<ParityAutomatonTree>(
                    new ParityAutomatonTreeBiconditionalNode(
                        node_type, parity, max_color, round_robin_size, parity_child, parity_child_max_color,
                        parity_child_index, std::move(children)));
        }
    }
}

Parity AutomatonTreeStructure::getParity() const {
    return tree->parity;
}

color_t AutomatonTreeStructure::getMaxColor() const {
    return tree->max_color;
}

product_state_t AutomatonTreeStructure::getInitialState() const {
    return initial_state;
}

ColorScore AutomatonTreeStructure::getSuccessor(const product_state_t& state, product_state_t& new_state, letter_t letter) {
    return tree->getSuccessor(state, new_state, letter);
}

BDD AutomatonTreeStructure::computeSafetyFilter(Cudd manager, size_t var_begin, size_t var_end) {
    return tree->computeSafetyFilter(manager, var_begin, var_end, 0);
}

std::vector<int> AutomatonTreeStructure::getAutomatonStates(const product_state_t& state) const {
    std::vector<int> automaton_states;
    automaton_states.reserve(leaf_state_indices.size());
    for (size_t index : leaf_state_indices) {
        node_id_t local_state = state[index];
        int owl_state;
        if (local_state == NODE_NONE) {
            throw std::invalid_argument("local state should never be NONE");
        }
        else if (local_state == NODE_BOTTOM || local_state == NODE_NONE_BOTTOM) {
            owl_state = -1;
        }
        else if (local_state == NODE_TOP || local_state == NODE_NONE_TOP) {
            owl_state = -2;
        }
        else {
            owl_state = local_state;
        }

        automaton_states.push_back(owl_state);
    }

    return automaton_states;
}

bool AutomatonTreeStructure::declareWinning(const product_state_t& state, const Player winner) {
    std::vector<int> automaton_states = getAutomatonStates(state);
    switch (winner) {
        case Player::SYS_PLAYER:
            return decomposed_dpa_declare_realizability_status(owl, owl_automaton, REALIZABLE, automaton_states.data(), automaton_states.size());
        case Player::ENV_PLAYER:
            return decomposed_dpa_declare_realizability_status(owl, owl_automaton, UNREALIZABLE, automaton_states.data(), automaton_states.size());
        default:
            return false;
    }
}

Player AutomatonTreeStructure::queryWinner(const product_state_t& state) {
    std::vector<int> automaton_states = getAutomatonStates(state);
    realizability_status_t status = decomposed_dpa_query_realizability_status(owl, owl_automaton, automaton_states.data(), automaton_states.size());
    switch (status) {
        case REALIZABLE:
            return Player::SYS_PLAYER;
        case UNREALIZABLE:
            return Player::ENV_PLAYER;
        default:
            return Player::UNKNOWN_PLAYER;
    }
}

bool AutomatonTreeStructure::isTopState(const product_state_t& state) const {
    return tree->isTopState(state);
}

bool AutomatonTreeStructure::isBottomState(const product_state_t& state) const {
    return tree->isBottomState(state);
}

std::set<letter_t> AutomatonTreeStructure::getAlphabet() const {
    return tree->getAlphabet();
}

atomic_proposition_status_t AutomatonTreeStructure::getVariableStatus(int variable) const {
    return statuses[variable];
}

void AutomatonTreeStructure::print(const int verbosity) const {
    tree->print(verbosity);
}

void AutomatonTreeStructure::print_memory_usage() const {
    for (const auto& automaton : automata) {
        automaton.print_memory_usage();
    }
}

}
