#include "ParityAutomatonTree.h"

#include <iostream>
#include <iomanip>
#include <algorithm>
#include <bitset>

namespace aut {

ParityAutomatonTreeLeaf::ParityAutomatonTreeLeaf(
        Automaton& _automaton,
        const std::string _name,
        const int _reference,
        std::vector<std::pair<letter_t,letter_t>> _mapping
) :
    ParityAutomatonTree(_automaton.getNodeType(), _automaton.getParity(), _automaton.getMaxColor()),
    automaton(_automaton),
    name(std::move(_name)),
    reference(std::move(_reference)),
    mapping(std::move(_mapping))
{
    automaton.setAlphabetSize(mapping.size());
}

ParityAutomatonTreeLeaf::~ParityAutomatonTreeLeaf() {}

void ParityAutomatonTreeLeaf::getInitialState(product_state_t& state) {
    // save index in product state
    state_index = state.size();
    // initial state of automaton
    state.push_back(0);
}

ColorScore ParityAutomatonTreeLeaf::getSuccessor(const product_state_t& state, product_state_t& new_state, letter_t letter) {
    if (isBottomState(state)) {
        setBottomState(new_state);
        return ColorScore(1 - parity, 0.0, 1.0);
    }
    else if (isTopState(state)) {
        setTopState(new_state);
        return ColorScore(parity, 1.0, 1.0);
    }

    const node_id_t local_state = state[state_index];

    letter_t new_letter = 0;

    for (const auto& map : mapping) {
        new_letter |= ((letter & ((letter_t)1 << map.first)) >> map.first) << map.second;
    }

    ScoredEdge edge = automaton.getSuccessor(local_state, new_letter);
    new_state[state_index] = edge.successor;

    return edge.cs;
}

BDD ParityAutomatonTreeLeaf::computeSafetyFilter(Cudd manager, size_t var_begin, size_t var_end, int depth) {
    // check if automaton only talks about specified variables
    bool correct_filter = true;
    std::vector<int> bdd_mapping(mapping.size());
    for (const auto& map : mapping) {
        if (var_begin <= map.first && map.first < var_end) {
            assert(map.second >= 0 && map.second < mapping.size());
            bdd_mapping[map.second] = map.first - var_begin;
        }
        else {
            correct_filter = false;
            break;
        }
    }
    if (correct_filter && automaton.hasSafetyFilter()) {
        return automaton.getSafetyFilter(manager, bdd_mapping);
    }
    else {
        return manager.bddOne();
    }
}

void ParityAutomatonTreeLeaf::setState(product_state_t& new_state, node_id_t state) {
    new_state[state_index] = state;
}

void ParityAutomatonTreeLeaf::setTopState(product_state_t& new_state) {
    new_state[state_index] = NODE_TOP;
}

void ParityAutomatonTreeLeaf::setBottomState(product_state_t& new_state) {
    new_state[state_index] = NODE_BOTTOM;
}

bool ParityAutomatonTreeLeaf::isTopState(const product_state_t& state) const {
    return state[state_index] == NODE_TOP;
}
bool ParityAutomatonTreeLeaf::isBottomState(const product_state_t& state) const {
    return state[state_index] == NODE_BOTTOM;
}

int ParityAutomatonTreeLeaf::getMinIndex() const {
    return reference;
}

letter_t ParityAutomatonTreeLeaf::getMaximumAlphabetSize() const {
    return mapping.size();
}

std::set<letter_t> ParityAutomatonTreeLeaf::getAlphabet() const {
    std::set<letter_t> alphabet;
    std::transform(mapping.begin(), mapping.end(),
            std::inserter(alphabet, alphabet.end()), [](const auto& map) -> letter_t { return map.first; });
    return alphabet;
}

void ParityAutomatonTreeLeaf::print(const int verbosity, const int indent) const {
    std::cout << std::setfill(' ') << std::setw(2*indent) << "";

    std::cout << state_index << ")";
    std::cout << " A[" << reference << "]";
    std::cout << " (";
    automaton.print_type();
    std::cout << ")";
    if (verbosity >= 2) {
        std::cout << " " << name;
    }
    std::cout << std::endl;
    if (verbosity >= 4) {
        for (const auto& map : mapping) {
            std::cout << std::setfill(' ') << std::setw(2*(indent+2)) << "" <<
                         "p" << map.first << " -> " << map.second << std::endl;
        }
    }
}

}
