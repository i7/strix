#pragma once

#include <queue>
#include <atomic>
#include <mutex>
#include <thread>
#include <condition_variable>

#include "cuddObj.hh"
#include "owltypes.h"
#include "libowl.h"

#include "Definitions.h"

namespace aut {

enum class NodeType : uint8_t {
    WEAK = 0,
    BUCHI = 1,
    CO_BUCHI = 2,
    PARITY = 3
};
enum class ParityType : uint8_t {
    MAX = 0,
    MIN = 1,
};
inline NodeType join_node_type(const NodeType a, const NodeType b) {
    return (NodeType)(static_cast<uint8_t>(a) | static_cast<uint8_t>(b));
}
inline NodeType join_node_type_biconditional(const NodeType a, const NodeType b) {
    if (join_node_type(a, b) == NodeType::WEAK) {
        return NodeType::WEAK;
    }
    else {
        return NodeType::PARITY;
    }
}

class Automaton {
    private:
        struct SuccessorCache {
            std::vector<int32_t> tree;
            std::vector<ScoredEdge> leaves;
            std::vector<ScoredEdge> direct;

            inline ScoredEdge tree_lookup(const letter_t letter) const {
                int32_t i;
                if (!tree.empty()) {
                    i = 0;
                    while (i >= 0) {
                        if ((letter & ((letter_t)1 << tree[i])) == 0) {
                            i = tree[i + 1];
                        } else {
                            i = tree[i + 2];
                        }
                    }
                }
                else {
                    i = -1;
                }
                return leaves[-i-1];
            }

            inline ScoredEdge direct_lookup(const letter_t letter) const {
                return direct[letter];
            }

            inline ScoredEdge lookup(const letter_t letter) const {
                if (direct.empty()) {
                    return tree_lookup(letter);
                }
                else {
                    return direct_lookup(letter);
                }
            }

            void flatten_tree(const letter_t max_letter) {
                direct.reserve(max_letter);
                for (letter_t letter = 0; letter < max_letter; letter++) {
                    direct.push_back(tree_lookup(letter));
                }
            }

            BDD toBDD(Cudd manager, std::vector<int>& bdd_mapping, int32_t pos) {
                if (pos < 0) {
                    node_id_t succ = leaves[-pos-1].successor;
                    if (succ == NODE_BOTTOM) {
                        return manager.bddZero();
                    }
                    else {
                        return manager.bddOne();
                    }
                }
                else {
                    int32_t var = tree[pos];
                    int32_t falseChild = tree[pos + 1];
                    int32_t trueChild = tree[pos + 2];
                    BDD bdd_var = manager.bddVar(bdd_mapping[var]);
                    BDD falseBDD = toBDD(manager, bdd_mapping, falseChild);
                    BDD trueBDD = toBDD(manager, bdd_mapping, trueChild);
                    return bdd_var.Ite(trueBDD, falseBDD);
                }
            }

            BDD toBDD(Cudd manager, std::vector<int>& bdd_mapping) {
                if (tree.empty()) {
                    return toBDD(manager, bdd_mapping, -1);
                }
                else {
                    return toBDD(manager, bdd_mapping, 0);
                }
            }
        };

        Automaton(const Automaton&) = delete;

        Isolate isolate;
        JVM owl;
        OwlAutomaton automaton;
        const NodeType node_type;
        const color_t max_color;
        const color_t default_color;
        const Parity parity;
        const ParityType parity_type;
        bool has_safety_filter;
        std::vector< SuccessorCache > successors;
        letter_t alphabet_size;
        letter_t max_number_successors;
        vector_int_t owl_nodes;
        vector_int_t owl_leaves;
        vector_double_t owl_scores;

        // queue for querying for successors
        std::queue<node_id_t> queries;

        // mutex for accessing the queue for queries
        std::mutex query_mutex;

        // mutex for accessing the successors
        std::mutex successors_mutex;

        // flag if the consumer thread currently has the lock on successors
        bool has_lock;

        // condition variable signalling new queries or complete construction
        std::condition_variable change;

        // condition variable signalling new successors
        std::condition_variable new_successors;

        // variable signalling that the automaton is completely constructed
        std::atomic<bool> complete;

        void add_new_query(node_id_t query);
        void wait_for_query(node_id_t& query);
        void add_new_states();
        void add_successors(node_id_t local_state);

        color_t initMaxColor() const;
        color_t initDefaultColor() const;
        NodeType initNodeType() const;
        Parity initParity() const;
        ParityType initParityType() const;
        bool initSafetyFilter() const;

    public:
        Automaton(Isolate isolate, JVM owl, OwlAutomaton automaton);
        Automaton(Automaton&&) = default;
        ~Automaton();

        void setAlphabetSize(const letter_t alphabet_size);
        letter_t getAlphabetSize() const;

        ScoredEdge getSuccessor(node_id_t local_state, letter_t letter);

        bool hasSafetyFilter();
        BDD getSafetyFilter(Cudd manager, std::vector<int>& bdd_mapping);

        color_t getMaxColor() const;
        NodeType getNodeType() const;
        Parity getParity() const;

        void print_type() const;
        void print_memory_usage() const;
};

}
