#include "Automaton.h"

#include <iostream>
#include <algorithm>

#include "util/SpecSeq.h"

namespace aut {

Automaton::Automaton(Isolate isolate, JVM owl, OwlAutomaton _automaton) :
    isolate(isolate),
    owl(owl),
    automaton(std::move(_automaton)),
    node_type(initNodeType()),
    max_color(initMaxColor()),
    default_color(initDefaultColor()),
    parity(initParity()),
    parity_type(initParityType()),
    has_safety_filter(initSafetyFilter()),
    alphabet_size(0),
    max_number_successors(0),
    has_lock(false),
    complete(false)
{
    // reserve some space in the vector to avoid initial resizing, which needs locking for the parallel construction
    successors.reserve(4096);

    owl_nodes.elements = nullptr;
    owl_leaves.elements = nullptr;
    owl_scores.elements = nullptr;
}

Automaton::~Automaton() {
    if (automaton != nullptr) {
        destroy_object_handle(owl, automaton);
        automaton = nullptr;
    }
}

void Automaton::setAlphabetSize(const letter_t _alphabet_size) {
    alphabet_size = _alphabet_size;
    max_number_successors = ((letter_t)1 << alphabet_size);
}

letter_t Automaton::getAlphabetSize() const {
    return alphabet_size;
}

color_t Automaton::initMaxColor() const {
    switch (automaton_acceptance_condition(owl, automaton)) {
        case PARITY_MIN_EVEN:
        case PARITY_MIN_ODD:
            return automaton_acceptance_condition_sets(owl, automaton) - 1;
        case PARITY_MAX_EVEN:
        case PARITY_MAX_ODD: {
                int c = automaton_acceptance_condition_sets(owl, automaton) - 1;
                c += c % 2;
                return c;
            }
        default:
            return 1;
    }
}

color_t Automaton::initDefaultColor() const {
    switch (automaton_acceptance_condition(owl, automaton)) {
        case CO_SAFETY:
            return 1;
        case SAFETY:
            return 0;
        default:
            return 0;
    }
}

NodeType Automaton::initNodeType() const {
    switch (automaton_acceptance_condition(owl, automaton)) {
        case PARITY_MIN_EVEN:
        case PARITY_MIN_ODD:
        case PARITY_MAX_EVEN:
        case PARITY_MAX_ODD:
            return NodeType::PARITY;
        case BUCHI:
            return NodeType::BUCHI;
        case CO_BUCHI:
            return NodeType::CO_BUCHI;
        case SAFETY:
            return NodeType::WEAK;
        case CO_SAFETY:
            return NodeType::WEAK;
        default:
            throw std::invalid_argument("unsupported acceptance");
    }
}

Parity Automaton::initParity() const {
    switch (automaton_acceptance_condition(owl, automaton)) {
        case PARITY_MIN_EVEN:
        case PARITY_MAX_EVEN:
        case BUCHI:
        case SAFETY:
        case CO_SAFETY:
            return Parity::EVEN;
        case PARITY_MIN_ODD:
        case PARITY_MAX_ODD:
        case CO_BUCHI:
            return Parity::ODD;
        default:
            throw std::invalid_argument("unsupported acceptance");
    }
}

bool Automaton::initSafetyFilter() const {
    return
        automaton_acceptance_condition(owl, automaton) == SAFETY &&
        automaton_is_singleton(owl, automaton);
}

ParityType Automaton::initParityType() const {
    switch (automaton_acceptance_condition(owl, automaton)) {
        case SAFETY:
        case CO_SAFETY:
        case BUCHI:
        case CO_BUCHI:
        case PARITY_MIN_EVEN:
        case PARITY_MIN_ODD:
            return ParityType::MIN;
        case PARITY_MAX_EVEN:
        case PARITY_MAX_ODD:
            return ParityType::MAX;
        default:
            throw std::invalid_argument("unsupported acceptance");
    }
}

color_t Automaton::getMaxColor() const {
    return max_color;
}

NodeType Automaton::getNodeType() const {
    return node_type;
}

Parity Automaton::getParity() const {
    return parity;
}

void Automaton::add_new_query(node_id_t query) {
    std::unique_lock<std::mutex> lock(query_mutex);
    queries.push(query);
    query_mutex.unlock();
    change.notify_all();
}

void Automaton::wait_for_query(node_id_t& query) {
    std::unique_lock<std::mutex> lock(query_mutex);
    while(!complete && queries.empty()) {
        change.wait(lock);
    }
    if (!queries.empty()) {
        query = queries.front();
        queries.pop();
    }
}

void Automaton::add_new_states() {
    while (!complete) {
        node_id_t query = NODE_BOTTOM;
        wait_for_query(query);
        if (query != NODE_BOTTOM) {
            add_successors(query);
        }
    }
}

void Automaton::add_successors(node_id_t local_state) {
    if (local_state >= successors.size()) {
        /*
        if (successors.capacity() >= local_state + 1) {
            successors_mutex.lock();
            has_lock = true;
        }
        */
        successors.resize(local_state + 1);
        /*
        if (has_lock) {
            successors_mutex.unlock();
            has_lock = false;
        }
        */
    }

    std::vector<int32_t>& tree = successors[local_state].tree;
    std::vector<ScoredEdge>& leaves = successors[local_state].leaves;
    if (leaves.empty()) {
        automaton_edge_tree(owl, automaton,
                local_state,
                &owl_nodes, &owl_leaves, &owl_scores);

        tree.reserve(owl_nodes.size);
        tree.insert(tree.end(), owl_nodes.elements, owl_nodes.elements + owl_nodes.size);

        leaves.reserve(owl_leaves.size);
        for (int i = 0; i < owl_leaves.size; i += 2) {
            node_id_t successor_state;
            int32_t state = owl_leaves.elements [i];
            int32_t color = owl_leaves.elements [i + 1];
            double score = owl_scores.elements [i/2];
            if (state == -1) {
                successor_state = NODE_BOTTOM;
                color = 1 - parity;
                score = 0.0;
            }
            else if (state == -2) {
                successor_state = NODE_TOP;
                color = parity;
                score = 1.0;
            }
            else {
                successor_state = state;
                if (node_type == NodeType::WEAK) {
                    color = default_color;
                }
                else {
                    // shift score so values 0.0 and 1.0 are excluded
                    score = 0.5*score + 0.25;
                    if (node_type == NodeType::BUCHI || node_type == NodeType::CO_BUCHI) {
                        if (color == -1) {
                            color = 1;
                        }
                        else {
                            color = 0;
                        }
                    }
                    else if (color == -1) {
                        color = max_color;
                    }
                }
            }
            if (parity_type == ParityType::MAX) {
                color = max_color - color;
            }
            leaves.push_back(ScoredEdge(successor_state, color, score, 1.0));
        }

        // free memory allocated by owl
        free_unmanaged_memory(owl, owl_nodes.elements);
        free_unmanaged_memory(owl, owl_leaves.elements);
        free_unmanaged_memory(owl, owl_scores.elements);
        owl_nodes.elements = nullptr;
        owl_leaves.elements = nullptr;
        owl_scores.elements = nullptr;

        // flatten tree for small alphabets
        if (alphabet_size <= 4) {
            successors[local_state].flatten_tree(max_number_successors);
        }

        //new_successors.notify_all();
    }
}

ScoredEdge Automaton::getSuccessor(node_id_t local_state, letter_t letter) {
    add_successors(local_state);
    /*
    std::unique_lock<std::mutex> lock(successors_mutex);
    while(local_state >= successors.size() || successors[local_state].empty()) {
        new_successors.wait(lock);
    }
    */
    return successors[local_state].lookup(letter);
}

bool Automaton::hasSafetyFilter() {
    return has_safety_filter;
}

BDD Automaton::getSafetyFilter(Cudd manager, std::vector<int>& bdd_mapping) {
    add_successors(0);
    return successors[0].toBDD(manager, bdd_mapping);
}

void Automaton::print_type() const {
    switch (node_type) {
        case NodeType::WEAK:
            if (default_color == 0) {
                std::cout << "Safety";
            }
            else {
                std::cout << "co-Safety";
            }
            break;
        case NodeType::BUCHI:
            std::cout << "Büchi";
            break;
        case NodeType::CO_BUCHI:
            std::cout << "co-Büchi";
            break;
        case NodeType::PARITY:
            std::cout << "Parity (" << getMaxColor() << ")";
            break;
    }
}

void Automaton::print_memory_usage() const {
    size_t cache_size = 0;
    for (const auto& succ : successors) {
        cache_size += succ.tree.size() * sizeof(int32_t);
        cache_size += succ.leaves.size() * sizeof(ScoredEdge);
        cache_size += succ.direct.size() * sizeof(ScoredEdge);
    }
    std::cout << "Automaton successors: " << (cache_size / 1024) << std::endl;
}

}
