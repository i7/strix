#pragma once

#include <string>

#include "Definitions.h"

#include "owltypes.h"
#include "libowl.h"

std::string owl_object_to_string(JVM owl, void* object);
