#!/usr/bin/env python3

import csv
import math
import sys
import re

max_number = 1000000000

if len(sys.argv) < 2:
    print("Usage: %s MODE RESULTSFILES..." % sys.argv[0])
    sys.exit(1)

results_realizability = False
results_synthesis = False
results_exploration = False
results_sizes = False
size_file = None
results_files = []

mode = sys.argv[1]
if mode == 'realizability':
    results_realizability = True
    results_files = sys.argv[2:]
elif mode == 'synthesis':
    results_synthesis = True
    results_files = sys.argv[2:]
elif mode == 'exploration':
    results_exploration = True
    results_files = sys.argv[2:]
elif mode == 'sizes':
    results_synthesis = True
    results_sizes = True
    size_file = sys.argv[2]
    results_files = sys.argv[3:]

results = []
print("\\begin{longtable}{lrr", end='');
if results_sizes:
    print("r", end='')
for results_file in results_files:
    print("r", end='')
    if results_synthesis or results_realizability:
        print("l", end='')
    if results_exploration:
        print("r", end='')
print("r}")

if results_exploration:
    n = len(results_files)
    print(" & \multicolumn{{{0}}}{{c}}{{{1: >3}}}".format(n, "States"), end='')
    print(" & \multicolumn{{{0}}}{{c}}{{{1: >3}}}".format(n, "Time"), end='')
    print("\\\\")
    print("\\cmidrule{{{}-{}}}".format(4, n+3))
    print("\\cmidrule{{{}-{}}}".format(n+4, 2*n+3))

for results_file in results_files:
    with open(results_file, 'r') as csvfile:
        reader = csv.DictReader(csvfile)
        results.append( { row['file']: row for row in reader } )

if results_sizes:
    with open(size_file, 'r') as csvfile:
        reader = csv.DictReader(csvfile)
        sizes = { row['file']: row for row in reader }

print("{0: <50}".format("Specification"), end='')
print(" & {0: >3}".format("$|I|$"), end='')
print(" & {0: >3}".format("$|O|$"), end='')
if results_sizes:
    print(" & {0: <8}".format("States"), end='')

for results_file in results_files:
    header = re.sub('results_|_realizability|_synthesis|\.csv$', '', results_file)
    if results_exploration or results_sizes:
        header = re.sub('strix_', '', header)
        header = re.sub('_plus', '+', header)
    header = re.sub('_', '\\_', header)
    if results_synthesis or results_realizability:
        print(" & \multicolumn{{2}}{{l}}{{{0: >3}}}".format(header), end='')
    else:
        print(" & {0: >10}".format(header), end='')

if results_exploration:
    for results_file in results_files:
        header = re.sub('strix_|results_|_realizability|_synthesis|\.csv$', '', results_file)
        header = re.sub('_plus', '+', header)
        header = re.sub('_', '\\_', header)
        print(" & {0: >10}".format(header), end='')

print(" & {0: <4}".format("diff"), end='')
print(" \\\\")

print("\\toprule")

# read reference sizes
reference = {}
with open('reference_sizes.csv', 'r') as csvfile:
    reader = csv.DictReader(csvfile)
    for reference_row in reader:
        filename = reference_row['file']
        realizability_status = reference_row['status_realizability']

        if not (results_synthesis and (
            realizability_status != 'realizable' or
            (results_sizes and filename not in sizes))
        ):
            diff = "---"

            if results_synthesis:
                max_size = 0
                min_size = max_number
                for r in results:
                    if filename in r:
                        row = r[filename]
                        size_result = row['size_aiger']
                        result = row['result']
                        if result == 'realizable':
                            try:
                                size = int(size_result)
                                min_size = min(min_size, size)
                                max_size = max(max_size, size)
                            except ValueError:
                                pass
                if min_size != max_number:
                    diff = int(100*math.log(float(max_size+1)/float(min_size+1))/math.log(10))
            elif results_exploration:
                max_states = 0
                min_states = max_number
                for r in results:
                    if filename in r:
                        row = r[filename]
                        states_result = row['states']
                        result = row['result']
                        if result == 'realizable' or result == 'unrealizable':
                            try:
                                states = int(states_result)
                                min_states = min(min_states, states)
                                max_states = max(max_states, states)
                            except ValueError:
                                pass
                if min_states != max_number:
                    diff = int(100*math.log(float(max_states)/float(min_states))/math.log(10))
            else:
                max_time = 0
                min_time = max_number
                for r in results:
                    if filename in r:
                        row = r[filename]
                        time_result = row['time']
                        result = row['result']
                        if result == 'realizable' or result == 'unrealizable':
                            try:
                                time = float(time_result)
                                min_time = min(min_time, time)
                                max_time = max(max_time, time)
                            except ValueError:
                                pass
                if min_time != max_number:
                    if max_time - min_time < 5.0:
                        diff = 0
                    else:
                        diff = int(100*math.log(float(max_time)/float(min_time))/math.log(10))

            if results_exploration:
                min_states = max_number
                for r in results:
                    if filename in r:
                        row = r[filename]
                        result = row['result']
                        if result == 'realizable' or result == 'unrealizable':
                            min_states = min(min_states, int(row['states']))

                break_line = True
                if min_states != max_number:
                    for r in results:
                        if filename not in r:
                            break_line = False
                            break
                        row = r[filename]
                        result = row['result']
                        if result != 'realizable' and result != 'unrealizable':
                            break_line = False
                            break
                        if int(row['states']) != min_states:
                            break_line = False
                            break
                if break_line:
                    continue

            output = re.sub('_', '\\_', re.sub('\.tlsf$', '', filename))
            output_format = "\\texttt{{{}}}".format(output)
            print("{0: <50}".format(output_format), end='')

            n_inputs = reference_row['n_inputs']
            n_outputs = reference_row['n_outputs']

            print(" & {0: >3}".format(n_inputs), end='')
            print(" & {0: >3}".format(n_outputs), end='')

            if results_sizes:
                states = sizes[filename]['states']
                print(" & {0: >8}".format(states), end='')

            if results_exploration:
                for r in results:
                    entry = "---"
                    if filename in r:
                        row = r[filename]
                        result = row['result']
                        if result == 'realizable' or result == 'unrealizable':
                            states = int(row['states'])
                            if states == min_states:
                                entry = "\\textbf{{{}}}".format(states)
                            else:
                                entry = states
                    print(" & {0: >10}".format(entry), end='')

            for r in results:
                entry1 = "---"
                entry2 = "\\xmark" if results_synthesis else None
                if filename in r:
                    row = r[filename]
                    result = row['result']
                    if result == 'memout':
                        entry1 = "\\memout"
                    elif result == 'timeout':
                        entry1 = "\\timeout"
                    elif result == 'error':
                        entry1 = "\\errorout"
                    elif result == 'realizable' or result == 'unrealizable':
                        if results_synthesis:
                            size_result = row['size_aiger']
                            if size_result == 'memout':
                                entry1 = "\\memout"
                            elif size_result == 'timeout':
                                entry1 = "\\timeout"
                            elif size_result == 'error':
                                entry1 = "\\errorout"
                            else:
                                size = int(size_result)
                                if size == min_size:
                                    entry1 = "\\textbf{{{}}}".format(size)
                                else:
                                    entry1 = size
                                verification_result = row['result_verification']
                                if verification_result == 'passed':
                                    entry2 = "\\cmark"
                                if verification_result == 'failed':
                                    entry2 = "\\fmark"
                        else:
                            time = float(row['time'])
                            entry1 = "{0:.1f}".format(time)
                            if results_realizability and time == min_time:
                                entry1 = "\\textbf{{{}}}".format(entry1)
                    else:
                        print("Unknown result: {}".format(result))
                        sys.exit(1)
                if entry1 is not None:
                    print(" & {0: >10}".format(entry1), end='')
                if entry2 is not None:
                    print(" & {0: >6}".format(entry2), end='')
                if results_realizability:
                    print(" &", end='')

            print(" & {0: <4}".format(diff), end='')

            print(" \\\\")

print("\\bottomrule")
print("\\end{longtable}");
