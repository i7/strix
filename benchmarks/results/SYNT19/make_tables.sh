#!/usr/bin/bash

function top10 {
    base=$1
    input=$2
    index=$3
    while read file rest; do
        esc=$(echo $file | sed -e 's/_/\\\\_/g')
        grep "$esc" $base | sed -e 's/^/\& /' -e 's/ & *[0-9]* *\\\\/ \\\\/'
    done < <(sort -n -k $index -r $input | head -n 10)
}

./make_table.py realizability results_strix_bfs_realizability.csv results_strix-cav18_realizability.csv results_ltlsynt_realizability.csv results_bosy_realizability.csv > table_realizability.tex

./make_table.py realizability results_strix_bfs_realizability.csv results_strix-cav18_realizability.csv > /tmp/table.tex
echo "Realizability top 8 for strix vs. strix (cav18)":
top10 table_realizability.tex /tmp/table.tex 13
echo

./make_table.py realizability results_strix_bfs_realizability.csv results_ltlsynt_realizability.csv > /tmp/table.tex
echo "Realizability top 8 for strix vs. ltlsynt":
top10 table_realizability.tex /tmp/table.tex 13
echo

./make_table.py realizability results_strix_bfs_realizability.csv results_bosy_realizability.csv > /tmp/table.tex
echo "Realizability top 8 for strix vs. bosy":
top10 table_realizability.tex /tmp/table.tex 13
echo

./make_table.py synthesis results_strix_bfs_auto_synthesis.csv results_strix-cav18_synthesis.csv results_ltlsynt_synthesis.csv results_bosy_synthesis.csv > table_synthesis.tex

./make_table.py synthesis results_strix_bfs_auto_synthesis.csv results_strix-cav18_synthesis.csv > /tmp/table.tex
echo "Synthesis top 8 for strix vs. strix (cav18)":
top10 table_synthesis.tex /tmp/table.tex 15
echo

./make_table.py synthesis results_strix_bfs_auto_synthesis.csv results_ltlsynt_synthesis.csv > /tmp/table.tex
echo "Synthesis top 8 for strix vs. ltlsynt":
top10 table_synthesis.tex /tmp/table.tex 15
echo

./make_table.py synthesis results_strix_bfs_auto_synthesis.csv results_bosy_synthesis.csv > /tmp/table.tex
echo "Synthesis top 8 for strix vs. bosy":
top10 table_synthesis.tex /tmp/table.tex 15
echo


./make_table.py exploration results_strix_bfs_realizability.csv results_strix_bfs_plus_realizability.csv results_strix_pq_realizability.csv results_strix_pq_plus_realizability.csv > table_exploration.tex

./make_table.py exploration results_strix_bfs_realizability.csv results_strix_pq_realizability.csv > /tmp/table.tex
echo "Exploration top 8 for bfs vs. pq":
top10 table_exploration.tex /tmp/table.tex 15
echo

./make_table.py exploration results_strix_bfs_realizability.csv results_strix_bfs_plus_realizability.csv > /tmp/table.tex
echo "Exploration top 8 for bfs vs. bfs+":
top10 table_exploration.tex /tmp/table.tex 15
echo

./make_table.py exploration results_strix_pq_realizability.csv results_strix_pq_plus_realizability.csv > /tmp/table.tex
echo "Exploration top 8 for pq vs. pq+":
top10 table_exploration.tex /tmp/table.tex 15
echo

./make_table.py exploration results_strix_bfs_plus_realizability.csv results_strix_pq_plus_realizability.csv > /tmp/table.tex
echo "Exploration top 8 for bfs+ vs. pq+":
top10 table_exploration.tex /tmp/table.tex 15
echo

./make_table.py exploration results_strix_bfs_plus_realizability.csv results_strix_pq_realizability.csv > /tmp/table.tex
echo "Exploration top 8 for bfs+ vs. pq":
top10 table_exploration.tex /tmp/table.tex 15
echo

./make_table.py exploration results_strix_bfs_realizability.csv results_strix_pq_plus_realizability.csv > /tmp/table.tex
echo "Exploration top 8 for bfs vs. pq+":
top10 table_exploration.tex /tmp/table.tex 15
echo

./make_table.py sizes results_strix_bfs_size_width.csv results_strix_bfs_unstructured_synthesis.csv results_strix_bfs_min_unstructured_synthesis.csv results_strix_bfs_structured_synthesis.csv results_strix_bfs_min_structured_synthesis.csv > table_sizes.tex

./make_table.py sizes results_strix_bfs_size_width.csv results_strix_bfs_unstructured_synthesis.csv results_strix_bfs_structured_synthesis.csv > /tmp/table.tex
echo "Sizes top 8 for unstr vs. struct":
top10 table_sizes.tex /tmp/table.tex 17
echo

./make_table.py sizes results_strix_bfs_size_width.csv results_strix_bfs_unstructured_synthesis.csv results_strix_bfs_min_unstructured_synthesis.csv > /tmp/table.tex
echo "Sizes top 8 for unstr vs. unstr^min":
top10 table_sizes.tex /tmp/table.tex 17
echo

./make_table.py sizes results_strix_bfs_size_width.csv results_strix_bfs_min_unstructured_synthesis.csv results_strix_bfs_min_structured_synthesis.csv > /tmp/table.tex
echo "Sizes top 8 for unstr^min vs. struct^min":
top10 table_sizes.tex /tmp/table.tex 17
echo

./make_table.py sizes results_strix_bfs_size_width.csv results_strix_bfs_structured_synthesis.csv results_strix_bfs_min_structured_synthesis.csv > /tmp/table.tex
echo "Sizes top 8 for struct vs. struct^min":
top10 table_sizes.tex /tmp/table.tex 17
echo

./make_table.py sizes results_strix_bfs_size_width.csv results_strix_bfs_unstructured_synthesis.csv results_strix_bfs_min_structured_synthesis.csv > /tmp/table.tex
echo "Sizes top 8 for unstr vs. struct^min":
top10 table_sizes.tex /tmp/table.tex 17
echo

./make_table.py sizes results_strix_bfs_size_width.csv results_strix_bfs_min_unstructured_synthesis.csv results_strix_bfs_structured_synthesis.csv > /tmp/table.tex
echo "Sizes top 8 for unstr^min vs. struct":
top10 table_sizes.tex /tmp/table.tex 17
echo

./crosscompare.py states 0 results_strix_bfs_realizability.csv results_strix_bfs_plus_realizability.csv results_strix_pq_realizability.csv results_strix_pq_plus_realizability.csv > table_exploration_cross_states.tex

./crosscompare.py time 5.0 results_strix_bfs_realizability.csv results_strix_bfs_plus_realizability.csv results_strix_pq_realizability.csv results_strix_pq_plus_realizability.csv > table_exploration_cross_time.tex
