#!/usr/bin/env python3

import csv
import math
import sys

if len(sys.argv) < 2:
    print("Usage: %s RESULTSFILE" % sys.argv[0])
    sys.exit(1)

results_file = sys.argv[1]
other_files = sys.argv[2:]

# read reference sizes
reference = {}
with open('reference_sizes.csv', 'r') as csvfile:
    reader = csv.DictReader(csvfile)
    for row in reader:
        reference[row['file']] = row

other_results = []
for other_file in other_files:
    with open(other_file, 'r') as csvfile:
        reader = csv.DictReader(csvfile)
        other_results.append( { row['file']: row for row in reader } )

realizability_solved = 0
realizability_unique = 0
synthesis_solved = 0
synthesis_unique = 0
quality_score = 0.0

with open(results_file, 'r') as csvfile:
    reader = csv.DictReader(csvfile)
    for row in reader:
        filename = row['file']
        result= row['result']
        reference_result= reference[filename]['status_realizability']
        reference_size_aiger= reference[filename]['size_aiger']

        if 'size_aiger' in row:
            size_aiger = row['size_aiger']
            result_verification = row['result_verification']
        else:
            size_aiger = None
            result_verification = None
        if result == reference_result:
            realizability_solved += 1
            unique = True
            for i, other in enumerate(other_results):
                if filename in other and other[filename]['result'] == reference_result:
                    unique = False
                    break
            if unique:
                realizability_unique += 1
            if result == 'realizable' and result_verification == 'passed':
                synthesis_solved += 1
                score = max(0, 2 - math.log((int(size_aiger) + 1) / (int(reference_size_aiger) + 1), 10))
                quality_score += score
                unique = True
                for other in other_results:
                    if filename in other and other[filename]['result'] == 'realizable' and other[filename]['result_verification'] == 'passed':
                        unique = False
                        break
                if unique:
                    synthesis_unique += 1

average_quality = 0.0
if synthesis_solved > 0:
    average_quality = quality_score / synthesis_solved

print("Realizability solved: {}".format(realizability_solved))
print("Realizability uniquely solved: {}".format(realizability_unique))
print("Synthesis solved: {}".format(synthesis_solved))
print("Synthesis uniquely solved: {}".format(synthesis_unique))
print("Quality: {}".format(quality_score))
print("Average Quality: {}".format(average_quality))
