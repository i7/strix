#!/usr/bin/bash

echo "Results for Strix (current version)"
echo
./evaluate_results.py results_strix_bfs_realizability.csv results_strix-cav18_realizability.csv results_ltlsynt_realizability.csv results_bosy_realizability.csv | head -n 2
./evaluate_results.py results_strix_bfs_auto_synthesis.csv results_strix-cav18_synthesis.csv results_ltlsynt_synthesis.csv results_bosy_synthesis.csv | tail -n+3
echo

echo "Results for Strix (CAV'18)"
echo
./evaluate_results.py results_strix-cav18_realizability.csv results_strix_bfs_realizability.csv results_ltlsynt_realizability.csv results_bosy_realizability.csv | head -n 2
./evaluate_results.py results_strix-cav18_synthesis.csv results_strix_bfs_auto_synthesis.csv results_ltlsynt_synthesis.csv results_bosy_synthesis.csv | tail -n+3
echo

echo "Results for ltlsynt"
echo
./evaluate_results.py results_ltlsynt_realizability.csv results_strix_bfs_realizability.csv results_strix-cav18_realizability.csv results_bosy_realizability.csv | head -n 2
./evaluate_results.py results_ltlsynt_synthesis.csv results_strix_bfs_auto_synthesis.csv results_strix-cav18_synthesis.csv results_bosy_synthesis.csv | tail -n+3
echo

echo "Results for BoSy"
echo
./evaluate_results.py results_bosy_realizability.csv results_strix_bfs_realizability.csv results_strix-cav18_realizability.csv results_ltlsynt_realizability.csv | head -n 2
./evaluate_results.py results_bosy_synthesis.csv results_strix_bfs_auto_synthesis.csv results_strix-cav18_synthesis.csv results_ltlsynt_synthesis.csv | tail -n+3
echo


echo "Results for Strix (unstructured)"
 ./evaluate_results.py results_strix_bfs_unstructured_synthesis.csv results_strix_bfs_min_unstructured_synthesis.csv results_strix_bfs_structured_synthesis.csv results_strix_bfs_min_structured_synthesis.csv | head -n 4 | tail -n 2
echo

echo "Results for Strix (min unstructured)"
 ./evaluate_results.py results_strix_bfs_min_unstructured_synthesis.csv results_strix_bfs_unstructured_synthesis.csv results_strix_bfs_structured_synthesis.csv results_strix_bfs_min_structured_synthesis.csv | head -n 4 | tail -n 2
echo

echo "Results for Strix (structured)"
 ./evaluate_results.py results_strix_bfs_structured_synthesis.csv results_strix_bfs_min_unstructured_synthesis.csv results_strix_bfs_unstructured_synthesis.csv results_strix_bfs_min_structured_synthesis.csv | head -n 4 | tail -n 2
echo

echo "Results for Strix (min structured)"
 ./evaluate_results.py results_strix_bfs_min_structured_synthesis.csv results_strix_bfs_min_unstructured_synthesis.csv results_strix_bfs_unstructured_synthesis.csv results_strix_bfs_structured_synthesis.csv | head -n 4 | tail -n 2
echo
