#!/usr/bin/env python3

import csv
import math
import sys
import re

if len(sys.argv) < 2:
    print("Usage: %s MODE THRESHOLD RESULTSFILES..." % sys.argv[0])
    sys.exit(1)

metric = sys.argv[1]
threshold = float(sys.argv[2])
results_files = sys.argv[3:]
results = []

for results_file in results_files:
    with open(results_file, 'r') as csvfile:
        reader = csv.DictReader(csvfile)
        results.append( { row['file']: row for row in reader } )

files = set()
with open('reference_sizes.csv', 'r') as csvfile:
    reader = csv.DictReader(csvfile)
    for row in reader:
        files.add(row['file'])

print("\\begin{longtable}{l", end='');
for results_file in results_files:
    print("r", end='')
print("r}")

print("{}".format(metric), end='')
for results_file in results_files:
    header = re.sub('strix_|results_|_realizability|_synthesis|\.csv$', '', results_file)
    header = re.sub('_', '\\_', header)
    print(" & {}".format(header), end='')
print(" & min \\\\")
print("\\toprule")

def get_metric(cur_results, f):
    if f in cur_results:
        result_row = cur_results[f]
        result = result_row['result']
        value = result_row[metric]
        if result == 'realizable' or result == 'unrealizable':
            try:
                num_value = float(value)
                return num_value
            except ValueError:
                print("No integer value for {}: {}".format(metric, value))
                return None
        else:
            return None
    else:
        return None

for i, results_file in enumerate(results_files):
    header = re.sub('strix_|results_|_realizability|_synthesis|\.csv$', '', results_file)
    header = re.sub('_', '\\_', header)
    print("{}".format(header), end='')
    for j, results_file in enumerate(results_files):
        if i != j:
            i_better = 0
            for f in files:
                i_metric = get_metric(results[i], f)
                j_metric = get_metric(results[j], f)
                if i_metric is not None and (j_metric is None or j_metric - i_metric > threshold):
                    i_better += 1

            print(" & {0: >5}".format(i_better), end='')
        else:
            print(" & ---", end='')

    i_better = 0
    for f in files:
        i_metric = get_metric(results[i], f)
        min_other = None
        for k, results_file in enumerate(results_files):
            if k != i:
                k_metric = get_metric(results[k], f)
                if k_metric is not None:
                    if min_other is None:
                        min_other = k_metric
                    else:
                        min_other = min(min_other, k_metric)
        if i_metric is not None and (min_other is None or min_other - i_metric > threshold):
            i_better += 1
    print(" & {0: >5}".format(i_better), end='')

    print(" \\\\")

print("\\bottomrule")
print("\\end{longtable}");
