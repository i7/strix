#!/bin/bash

CONTROLLER_PREFIX="controllers_"
declare -a TOOLS=("strix_bfs_auto" "strix-cav18" "ltlsynt" "bosy")

function size_of_aig {
    num_latches=$(head -n 1 $1 | cut -d ' ' -f 4)
    num_and_gates=$(head -n 1 $1 | cut -d ' ' -f 6)
    size=$((num_latches + num_and_gates))
}

MAX_SIZE=1000000000

echo "file,n_inputs,n_outputs,status_realizability,size_aiger,tool"

OLDIFS=$IFS
IFS=,
while read file realizability_status rest; do
    SRC=../../SYNTCOMP2019/$file
    n_inputs=$(syfco $SRC --print-input-signals | sed -e 's/,//g' | wc -w)
    n_outputs=$(syfco $SRC --print-output-signals | sed -e 's/,//g' | wc -w)

    AAG=${file%.tlsf}.aag
    min_size=$MAX_SIZE
    min_tool="-"
    for tool in "${TOOLS[@]}"; do
        AAG_FILE="${CONTROLLER_PREFIX}${tool}/${AAG}"
        if [ -f $AAG_FILE ]; then
            size_of_aig $AAG_FILE
            if [ $size -lt $min_size ]; then
                min_size=$size
                min_tool=$tool
            fi
        fi
    done
    if [ $min_size -eq $MAX_SIZE ]; then
        min_size="-"
    fi
    echo "$file,$n_inputs,$n_outputs,$realizability_status,$min_size,$min_tool"
done < realizability_status.csv
IFS=$OLDIFS
